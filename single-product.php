<?php get_header(); ?>

<?php
    global $woocommerce;
    $product = new WC_Product( get_the_ID() );
?>

<main>
    <section class="product">
        <div class="wrapper">
            
        <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

            <div class="product__box">
                <div class="product__column">
                    <?php
                        if ( has_post_thumbnail( $post->ID )) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                        } else {
                            $image = get_option( 'my_default_pic' );
                        }
                    ?>

                    <div style="background-image: url('<?php echo $image; ?>');" class="product__cover"></div>
                </div>
                <div class="product__column">
                    <h1 class="product__title"><?php the_title(); ?></h1>

                    <div class="product__meta">
                        <div class="product__meta-column">
                            <strong class="product__old-price"><?php echo $product->get_regular_price(); ?> BYN</strong>
                            <strong class="product__price"><?php echo $product->get_sale_price(); ?> BYN</strong>
                        </div>
                    </div>
                    
                    <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="" aria-label="Добавить &quot;Бор&quot; в корзину" rel="nofollow">
                        <button style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/basket-white.png');" class="product__button">В корзину</button>
                    </a>
                    
                    <h3 class="product__subtitle">Описание</h3>
                    <div class="product__description"><?php the_content(); ?></div>
                </div>
            </div>
        </div>            
    </section>

    <br><br><br><br><br><br>

    <section class="hit">
        <div class="wrapper">

            <h3 class="section__title section__title_md">Возможно вам понравится</h3>

            <div class="hit__box">

                <?php
                    $args = array(
                        'post_type' => 'product',
                        'post_status' => 'publish',
                        'posts_per_page' => 3,
                        'orderby' => 'rand',
                    );                

                    $query = new WP_Query( $args );
                ?>

                <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <div class="hit__column">
                    <?php
                        if ( has_post_thumbnail( $post->ID )) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                        } else {
                            $image = get_option( 'my_default_pic' );
                        }
                    ?>

                    <div style="background-image: url('<?php echo $image; ?>');" class="hit__cover"></div>

                    <a class="hit__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                    <div class="hit__meta">
                        <div class="hit__meta-column">
                            <strong class="hit__old-price"><?php echo $product->get_regular_price(); ?> BYN</strong>
                            <strong class="hit__price"><?php echo $product->get_sale_price(); ?> BYN</strong>
                        </div>
                        <div class="hit__meta-column">
                            <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="" aria-label="Добавить &quot;Бор&quot; в корзину" rel="nofollow">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-basket.svg" alt="<?php the_title(); ?>" srcset="">
                            </a>
                        </div>
                    </div>
                </div>
                <?php endwhile; wp_reset_postdata(); ?>

            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>