<?php
/*
Template Name: Feedback
*/

get_header();
?>

<section class="feedback">
    <div class="wrapper">
        <h3 class="section__title">Отзывы</h3>
        <div class="feedback__comments">
            <?php comments_template(); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>