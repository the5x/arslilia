<?php get_header(); ?>

<main> 
    
    <section data-aos="flip-up" data-aos-duration="2000" class="slider">
        <div class="wrapper">
            <img src="<?php echo get_template_directory_uri(); ?>/images/slider.png" alt="" srcset="">
        </div>
    </section>

    <section class="category">
        
        <img class="category__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-category.svg" alt="" srcset="">

        <div class="wrapper">
            <h3 class="section__title">
                <a class="section__title-link" href="<?php echo site_url(); ?>/categories">Каталог продукции</a>
            </h3>
            
            <div class="category__box">

                <?php
                    $orderby = 'name';
                    $order = 'asc';
                    $hide_empty = false;

                    $args = array(
                        'orderby' => $orderby,
                        'order' => $order,
                        'hide_empty' => $hide_empty,
                    );

                    $product_categories = get_terms( 'product_cat', $args );
                ?>

                <?php foreach( $product_categories as $category ) :?>
                    <div class="category__item">                        
                        <?php
                            $category_thumb_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                            $category_cover = wp_get_attachment_url( $category_thumb_id, 'shop_catalog' );
                        ?>

                        <img class="category__item-cover" src="<?php echo $category_cover; ?>" alt="">

                        <a class="category__title" href="<?php echo esc_attr( get_category_link( $category->term_id )); ?>"><?php echo $category->name ;?></a>
                    </div>

                <?php endforeach; wp_reset_postdata(); ?>
                
            </div>
            
        </div>
    </section>

    <section class="hit">
        <div class="wrapper">
            
            <img class="hit__ico-flower" src="<?php echo get_template_directory_uri(); ?>/images/ico-hit-flower.png" alt="" srcset="">

            <h3 class="section__title">Хит продаж</h3>

            <div class="hit__box">
                <?php
                    $args = array(
                        'post_type' => 'product',
                        'posts_per_page' => 6,
                        'orderby' => 'meta_value',
                        'meta_key' => '_sale_price',
                        'order' => 'ASC',

                        'tax_query' => array( array(
                            'taxonomy' => 'product_visibility',
                            'field'    => 'name',
                            'terms'    => 'featured',
                        ))
                    );
                    
                    $query = new WP_Query($args);
        
                    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
                ?>

                <div class="hit__column">
                    <div class="hit__label pulse">ХИТ</div>
                    <div style="background-image: url('<?php echo wp_get_attachment_url( $product->get_image_id() ); ?>" class="hit__cover"></div>

                    <a class="hit__title" href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 55, '...'); ?></a>

                    <div class="hit__meta">
                        <div class="hit__meta-column">
                            <strong class="hit__old-price"><?php echo $product->get_regular_price(); ?> BYN</strong>
                            <strong class="hit__price"><?php echo $product->get_sale_price(); ?> BYN</strong>
                        </div>
                        <div class="hit__meta-column">                            
                            <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="" aria-label="Добавить &quot;Бор&quot; в корзину" rel="nofollow">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/ico-basket.svg" alt="<?php the_title(); ?>" srcset="">
                            </a>
                        </div>
                    </div>
                </div>

                <?php endwhile;  wp_reset_postdata(); else : ?>
                    <div class="hit__column message"><?php esc_html_e( 'Товары данной категории отсутствуют' ); ?></div>
                <?php endif; ?>

            </div>

        </div>
    </section>


    <section class="step">

        <img class="step__ico-flower" src="<?php echo get_template_directory_uri(); ?>/images/ico-step-flower.png" alt="" srcset="">

        <div class="wrapper">

            <h3 class="section__title">Как сделать заказ</h3>

            <div class="step__box">
                
                <div class="step__item">
                    <div class="step__round step__round_light">
                        <img class="step__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-step-01.svg" alt="" srcset="">
                    </div>

                    <span class="step__description">
                        Посмотрите наш огромный каталог цветов
                    </span>
                </div>


                <div class="step__item">
                    <div class="step__round step__round_mt">
                        <img class="step__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-step-02.svg" alt="" srcset="">
                    </div>

                    <span class="step__description">
                        Добавьте понравившиеся цветы в корзину
                    </span>
                </div>


                <div class="step__item">
                    <div class="step__round step__round_light">
                        <img class="step__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-step-03.svg" alt="" srcset="">
                    </div>

                    <span class="step__description">
                        Заполните информацию о доставке
                    </span>
                </div>


                <div class="step__item">
                    <div class="step__round step__round_mt">
                        <img class="step__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-step-04.svg" alt="" srcset="">
                    </div>

                    <span class="step__description">
                        Дождитесь нашего скоростного курьера
                    </span>
                </div>


                <div class="step__item">
                    <div class="step__round step__round_light">
                        <img class="step__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-step-05.svg" alt="" srcset="">
                    </div>

                    <span class="step__description">
                        Насладитесь красотой
                    </span>
                </div>
            </div>

        </div>
    </section>


    <section class="stock">
        <div class="wrapper">
            <h3 class="section__title">
                <a class="section__title-link" href="<?php echo site_url(); ?>/stocks">Акции</a>
            </h3>

            <div class="swiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->

                    <?php
                        $args = array(
                            'post_type' => 'stocks',
                            'posts_per_page' => -1,
                        );
                        
                        $query = new WP_Query($args);
            
                        $the_count = $query->found_posts;

                        if ( $query->have_posts() && $the_count > 1 ) : while ( $query->have_posts() ) : $query->the_post();
                    ?>

                    <div class="swiper-slide">
                        <?php
                            if ( has_post_thumbnail( $post->ID )) {
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                            } else {
                                $image = get_option( 'my_default_pic' );
                            }
                        ?>

                        <img class="stock__pic" src="<?php echo $image; ?>" alt="<?php the_title(); ?>" srcset="">
                    </div>

                    <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>

                <!-- If we need navigation buttons -->
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                
                </div>

        </div>
    </section>


    <section class="about">
        
        <img class="about__ico-flower" src="<?php echo get_template_directory_uri(); ?>/images/ico-about-flower.png" alt="" srcset="">

        <div class="wrapper">
            <div class="about__box">
                <div data-aos="fade-up" data-aos-duration="4000" class="about__item">
                    <h3 class="section__title">
                        <a class="section__title-link" href="<?php echo site_url(); ?>/about">О нас</a>
                    </h3>

                    <p class="about__content">«АрсЛилия» — это не просто интернет-магазин с бесплатной доставкой цветов по Гродно. Мы работаем и как интернет-магазин комнатных цветов и растений.</p>
                    <p class="about__content">В нашем интернет-магазине домашних цветов в горшках вы найдете декоративные лиственные растения и цветущие. От привычных фиалок, калл и фикусов до шикарных цветущих бегоний, азалий и необычных деревьев-бонсай.</p>
                </div>
                <div data-aos="fade-up" data-aos-duration="2000" class="about__item">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/about.png" alt="" srcset="">
                </div>
            </div>
        </div>
    </section>


    <section class="advantage">
        
        <img class="advantage__ico-flower" src="<?php echo get_template_directory_uri(); ?>/images/ico-advantage-flower.png" alt="" srcset="">

        <div class="wrapper">
            <ul style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/pion.png);" class="advantage__list">
                <li class="advantage__list-item">
                    <strong class="advantage__number">01</strong>
                    <strong class="advantage__title">10 лет на рынке</strong>
                </li>
                <li class="advantage__list-item advantage__list-item_empty"></li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">02</strong>
                    <strong class="advantage__title">Ежедневное обновление цветов</strong>
                </li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">03</strong>
                    <strong class="advantage__title">Более 50 сортов роз</strong>
                </li>
                <li class="advantage__list-item advantage__list-item_empty"></li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">04</strong>
                    <strong class="advantage__title">Букеты из экзотических цветов</strong>
                </li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">05</strong>
                    <strong class="advantage__title">Бесплатная доставка от 50 byn</strong>
                </li>
                <li class="advantage__list-item advantage__list-item_empty"></li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">06</strong>
                    <strong class="advantage__title">Удобное расположение магазинов</strong>
                </li>
            </ul>
        </div>
    </section>


    <section class="trust">
        <div class="wrapper">

            <img class="trust__ico-flower" src="<?php echo get_template_directory_uri(); ?>/images/ico-trust-flower.png" alt="" srcset="">

            <h3 class="section__title">Нам доверяют</h3>

            <div class="swiper swiper__trust">

                <div class="swiper-wrapper">

                    <div class="swiper-slide">
                        <img class="stock__pic" src="<?php echo get_template_directory_uri(); ?>/images/logo-01.jpg" alt="" srcset="">
                    </div>
                    <div class="swiper-slide">
                        <img class="stock__pic" src="<?php echo get_template_directory_uri(); ?>/images/logo-02.jpg" alt="" srcset="">
                    </div>
                    <div class="swiper-slide">
                        <img class="stock__pic" src="<?php echo get_template_directory_uri(); ?>/images/logo-03.jpg" alt="" srcset="">
                    </div>
                    <div class="swiper-slide">
                        <img class="stock__pic" src="<?php echo get_template_directory_uri(); ?>/images/logo-04.jpg" alt="" srcset="">
                    </div>
                    <div class="swiper-slide">
                        <img class="stock__pic" src="<?php echo get_template_directory_uri(); ?>/images/logo-05.jpg" alt="" srcset="">
                    </div>
                    <div class="swiper-slide">
                        <img class="stock__pic" src="<?php echo get_template_directory_uri(); ?>/images/logo-01.jpg" alt="" srcset="">
                    </div>
                </div>
        
                </div>


        </div>
    </section>


    <section class="feedback">
        <div class="wrapper">

            <h3 class="section__title">
                <a class="section__title-link" href="<?php echo site_url(); ?>/feedback">Отзывы</a>
            </h3>

            <div class="feedback__box">
            <?php
                $args = array(
                    'number' => 2,
                    'offset' => 0,
                    'status' => 'approve',
                );
            ?>
            <?php
                $num_comments = get_comments( $args );
            ?>
            <?php if ( count( $num_comments )  > 1 ) : foreach ( get_comments( $args ) as $comment ): ?>
                <div class="feedback__item">
                    <time class="feedback__time"><?php echo date('d.m.Y', strtotime( $comment->comment_date )); ?></time>
                    <strong class="feedback__title"><?php echo $comment->comment_author; ?></strong>

                    <p class="feedback__info"><?php echo $comment->comment_content; ?></p>
                </div>
            <?php endforeach; endif; ?>
            </div>
        </div>
    </section>

</main>

<?php get_footer(); ?>

<script type="text/javascript">

const swiper = new Swiper('.swiper', {

    loop: true,
    slidesPerView: 2,
    spaceBetween: 30,
    speed: 300,
    pagination: {
        el: '.swiper-pagination',
    },

    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },

});


const swiperTrust = new Swiper('.swiper__trust', {

    loop: true,
    speed: 300,
    slidesPerView: 5,
    spaceBetween: 200,
    pagination: {
        el: '.swiper-pagination',
    },

    autoplay: {
        delay: 2500,
        disableOnInteraction: false,
        reverseDirection: false,
    },

});

</script>