<?php
/*
Template Name: Все категории
*/
?>

<?php get_header(); ?>

<main> 
    <section class="category category_pt0">
        
        <img class="category__ico" src="<?php echo get_template_directory_uri(); ?>/images/ico-category.svg" alt="" srcset="">

        <div class="wrapper">

            <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

            <h3 class="section__title">Каталог продукции</h3>
            
            <div class="category__box">

                <?php

                $orderby = 'name';
                $order = 'asc';
                $hide_empty = false;

                $args = array(
                    'orderby' => $orderby,
                    'order' => $order,
                    'hide_empty' => $hide_empty,
                );

                $product_categories = get_terms( 'product_cat', $args );
                ?>


                <?php foreach($product_categories as $category) :?>

                    <a class="category__title" href="<?php echo esc_attr( get_category_link( $category->term_id )); ?>">

                    <div class="category__item category__item_mw33">
                        <?php
                            $category_thumb_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                            $category_cover = wp_get_attachment_url( $category_thumb_id, 'shop_catalog' );
                        ?>

                        <img class="category__item-cover" src="<?php echo $category_cover; ?>" alt="">

                        <a class="category__title" href="<?php echo esc_attr( get_category_link( $category->term_id )); ?>"><?php echo $category->name ;?></a>
                    </div>

                    </a>
                    

                <?php endforeach; ?>
                
            </div>
            
        </div>
    </section>

</main>

<?php get_footer(); ?>
