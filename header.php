<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package arslilia
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css"/>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<header class="header">
    <div class="wrapper">
        <div class="header__box">
            <div class="header__item">
                <?php get_search_form(); ?>
            </div>
            <div data-aos="fade-up" data-aos-duration="500" class="header__item">
                <a class="logo" href="<?php echo site_url(); ?>">арсилилия</a>
            </div>
            <div class="header__item">
                <a class="header__phone" href="tel:+375333992630">+375 (33) 399-26-30</a>
            </div>
            <div class="header__item header__item_small">
                <a class="header__cart" href="<?php echo site_url(); ?>/cart">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/ico-basket.svg" alt="" sizes="" srcset="">                                                
                    <span class="counter" id="cart-count">                            
                        <?php
                            $cart_count = WC()->cart->get_cart_contents_count();
                            echo sprintf ( _n( '%d', '%d', $cart_count ), $cart_count );
                        ?>
                    </span>
                </a>
            </div>
        </div>
    </div>
</header>

<nav class="nav">
    <div class="wrapper">
        <ul class="nav__list">
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/categories">Каталог</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/about">О нас</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/stocks">Акции</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/payment">Оплата</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/delivery">Доставка</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/blog">Блог</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/feedback">Отзывы</a></li>
            <li class="nav__list-item"><a class="nav__list-item-link" href="<?php echo site_url(); ?>/contacts">Контакты</a></li>
            <li class="nav__list-item">

                <ul class="social__list">
                    <li class="social__list-item">
                        <a class="social__list-item-link" href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-vk.svg" alt="" srcset="">
                        </a>
                    </li>
                    <li class="social__list-item">
                        <a class="social__list-item-link" href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-fb.svg" alt="" srcset="">
                        </a>
                    </li>
                    <li class="social__list-item">
                        <a class="social__list-item-link" href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-social-in.svg" alt="" srcset="">
                        </a>
                    </li>
                </ul>
    
            </li>
        </ul>
    </div>
</nav>
