<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package arslilia
 */

get_header();
?>

	<main id="primary" class="site-main">

		<?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title">
					<?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Результат поиска: %s', 'arslilia' ), '<span>' . get_search_query() . '</span>' );
					?>
				</h1>
			</header><!-- .page-header -->
			<br><br><br>
			<?php while ( have_posts() ) : the_post(); ?>

			<?php 
				if ( has_post_thumbnail( $post->ID )) {
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
				} else {
					$image = get_option( 'my_default_pic' );
				}
            ?>

			<?php if ( isset( $image ) ): ?>
				<div class="search__box">
					<div class="search__item search__item_mr5">
						<img class="search__cover" src="<?php echo $image;?>" alt="<?php the_title(); ?>">
					</div>
					<div class="search__item">
						<a class="search__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<div class="product__description"><?php echo wp_trim_words( get_the_content(), 25, '...'); ?></div>
					</div>
				</div>
			<?php else: ?>
				<div class="search__box">
					<div class="search__item">
						<a class="search__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						<div class="product__description"><?php echo wp_trim_words( get_the_content(), 25, '...'); ?></div>
					</div>
				</div>
			<?php endif ?>
			
			<?php endwhile; the_posts_navigation(); ?>
		</div>
		<?php else : ?>

			<h3 class="search__title">По вашему запросу ничего не найдено</h3>

		<?php endif; ?>

	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
