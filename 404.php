<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package arslilia
 */

get_header();
?>

	<div class="wrapper">
		<h1 class="section__title"><a class="section__title-link" href="<?php echo site_url(); ?>">По вашему запросу ничего не найдено. Попробуйте зайти сюда позже...</a></h1>
	</div>

<?php
get_footer();
