<?php get_header(); ?>

<?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    // Стартовый каталог для категории
    $args = array(
        'post_type' => 'product',
        'post_status' => 'publish',
        'posts_per_page' => 12,
        'paged' => $paged,
        
        'tax_query' => array( array(
            'taxonomy' => 'product_cat',
            'field' => 'term_id',
            'terms' => array( get_queried_object()->term_id ),
            'operator'  => 'IN'
        ) )
    );

    // Сортировка товара dropdown
    if ( isset( $_GET['orderby'] ) && $_GET['orderby'] !== "" ) {
        $orderby = 'ID';
        $order = 'DESC';
    
        $order_by = $_GET['orderby'];

                WC()->query->remove_ordering_args();

        if ($order_by == 'title-desc') {
            $orderby = 'title';
            $order = 'DESC';
        }

        if ($order_by == 'title-asc') {
            $orderby = 'title';
            $order = 'ASC';
        }

        if ($order_by == 'price-asc') {
            $meta_key = '_price';
            $orderby  = ['meta_value_num' => 'ASC', $order => 'ASC'];
        }

        if ($order_by == 'price-desc') {
            $meta_key = '_price';
            $orderby  = ['meta_value_num' => 'DESC', $order => 'DESC'];
        }

        if ($order_by == 'date-asc') {
            $orderby = 'ID';
            $order = 'ASC';
        }

        if ($order_by == 'date-desc') {
            $orderby = 'ID';
            $order = 'DESC';
        }

        if ( $meta_key == '_price' ) {
        
            $args = array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'posts_per_page' => 12,
                'paged' => $paged,
                'orderby' => $orderby,
                'order' => $order,
                'meta_key' => $meta_key,

                'tax_query' => array( array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => array( get_queried_object()->term_id ),
                    'operator'  => 'IN'
                ) )
            );
    
        } else {
            
            $args = array(
                'post_type' => 'product',
                'post_status' => 'publish',
                'posts_per_page' => 12,
                'paged' => $paged,
                'orderby' => $orderby,
                'order' => $order,

                'tax_query' => array( array(
                    'taxonomy' => 'product_cat',
                    'field' => 'term_id',
                    'terms' => array( get_queried_object()->term_id ),
                    'operator'  => 'IN'
                ) )
            );
    
        }
    }

    // Сортировка по цене: минимальная и максимальная
    if ( isset( $_GET['min'], $_GET['max'] ) ) {
        
        $min_price = $_GET['min'];
        $max_price = $_GET['max'];

        $args = array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'posts_per_page' => 12,
            'paged' => $paged,
            'orderby' => 'meta_value',
            'order' => 'DESC',
            
            'meta_query' => array( array(
                'key' => '_price',
                'value' => array( $min_price, $max_price ),
                'compare' => 'BETWEEN',
                'type' => 'NUMERIC'
            ) ),
        );
    }

    $query = new WP_Query( $args );
?>


<div class="control">
    <div class="wrapper">
        
        <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

        <?php

            $banner_args = array(
                'post_type' => 'stocks',
                'posts_per_page' => 1,
                'orderby' => 'rand',

                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'term_id',
                        'terms' => get_queried_object()->term_id,
                    )
                )
            );

            $banner_query = new WP_Query($banner_args);

            if ( $banner_query->have_posts() ) : while ( $banner_query->have_posts() ) : $banner_query->the_post();
        ?>


        <?php
            if ( has_post_thumbnail( $post->ID )) {
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
            } else {
                $image = get_option( 'my_default_pic' );
            }
        ?>

        <img data-aos="flip-up" data-aos-duration="2000" class="control__banner" src="<?php echo $image; ?>" alt="<?php the_title(); ?>" srcset="">

        <?php endwhile; endif; wp_reset_postdata(); ?>

        <div class="control__box">
            <div class="control__item">
                <form class="control__form" method="GET">
                    <div class="control__column">
                        <label class="control__label" for="min">Мин</label>
                        <input class="control__input" min="0" value="<?php echo $_GET['min'] ?? 0 ?>" name="min" type="number">
                    </div>

                    <div class="control__column">
                        <label class="control__label" for="max">Макс</label>
                        <input class="control__input" min="0" value="<?php echo $_GET['max'] ?? 0 ?>" name="max" type="number">
                    </div>
                    <input class="control__submit" type="submit" value="Подобрать">
                </form>
            </div>
            <div class="control__item">
                <form method="GET">
                    <div class="control__column">

                        <select class="control__input control__input_wa" name="orderby" onchange="this.form.submit()">

                            <option <?php if (isset($order_by) && $orderby == 'ID' && isset($order) && $order == 'DESC') {
                                        echo 'selected';
                                    } ?> value="date-desc">Новые
                            </option>

                            <option <?php if (isset($order_by) && $orderby == 'ID' && isset($order) && $order == 'ASC') {
                                        echo 'selected';
                                    } ?> value="date-asc">Старые
                            </option>

                            <option <?php if (isset($order_by) && $orderby == 'title' && isset($order) && $order == 'ASC') {
                                        echo 'selected';
                                    } ?> value="title-asc">от А до Я
                            </option>

                            <option <?php if (isset($order_by) && $orderby == 'title' && isset($order) && $order == 'DESC') {
                                        echo 'selected';
                                    } ?> value="title-desc">от Я до А
                            </option>

                            <option <?php if (isset($order_by) && $orderby == ['meta_value_num' => 'ASC', $order => 'ASC'] && isset($meta_key) && $meta_key == '_price') {
                                        echo 'selected';
                                    } ?> value="price-asc">Сначала низкая цена
                            </option>

                            <option <?php if (isset($order_by) && $orderby == ['meta_value_num' => 'DESC', $order => 'DESC']  && isset($meta_key) && $meta_key == '_price') {
                                        echo 'selected';
                                    } ?> value="price-desc">Сначала высокая цена
                            </option>
                        </select>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<main>    
    <section class="hit">
        <div class="wrapper">

            <h3 class="section__title"><?php single_cat_title(); ?></h3>

            <div class="hit__box">

            <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            
                <div class="hit__column">
                    <?php
                        if ( has_post_thumbnail( $post->ID )) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                        } else {
                            $image = get_option( 'my_default_pic' );
                        }
                    ?>

                    <div style="background-image: url('<?php echo $image; ?>');" class="hit__cover"></div>

                    <a class="hit__title" href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 55, '...'); ?></a>

                    <div class="hit__meta">
                        <div class="hit__meta-column">
                            <strong class="hit__old-price"><?php echo $product->get_regular_price(); ?> BYN</strong>
                            <strong class="hit__price"><?php echo $product->get_sale_price(); ?> BYN</strong>
                        </div>
                        <div class="hit__meta-column">
                            <a href="?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo get_the_ID(); ?>" data-product_sku="" aria-label="Добавить &quot;Бор&quot; в корзину" rel="nofollow">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/ico-basket.svg" alt="<?php the_title(); ?>" srcset="">
                            </a>
                        </div>
                    </div>
                </div>

            <?php endwhile; wp_reset_postdata(); ?>

            </div>
            
            <div class="pagination">
                <?php the_posts_pagination(); ?>
            </div>

            <br><br><br><br><br>

            <?php
                //show category description
                $term_object = get_queried_object();
            ?>

            <h3 data-aos="fade-up" data-aos-duration="3000" class="section__title section__title_sm">Описание раздела</h3>
            <div data-aos="fade-up" data-aos-duration="3000" class="content"><?php echo $term_object->description ? $term_object->description : 'Описание раздела пока не заполненно...' ; ?></p>

        </div>        
    </section>
</main>


<?php get_footer(); ?>