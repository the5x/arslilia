<footer class="footer">
    <div class="wrapper">
        
        <a href="#" class=logo>Арслилия</a>

        <div class="footer__box">
            <div class="footer__item">

                <a class="footer__link" href="#">Главная</a>
                <a class="footer__link" href="#">О нас</a>
                <a class="footer__link" href="#">Каталог</a>
                <a class="footer__link" href="#">Акции</a>
                <a class="footer__link" href="#">Оплата</a>
                <a class="footer__link" href="#">Доставка</a>
                <a class="footer__link" href="#">Блог</a>
                <a class="footer__link" href="#">Контакты</a>

                <br><br>

                <ul class="social__list social__list_mlauto">
                    <li class="social__list-item social__list-item_mr3">
                        <a class="social__list-item-link" href="#">
                            <img src="http://localhost/wordpress/arslilia/wp-content/themes/arslilia/images/ico-social-vk.svg" alt="" srcset="">
                        </a>
                    </li>
                    <li class="social__list-item social__list-item_mr3">
                        <a class="social__list-item-link" href="#">
                            <img src="http://localhost/wordpress/arslilia/wp-content/themes/arslilia/images/ico-social-fb.svg" alt="" srcset="">
                        </a>
                    </li>
                    <li class="social__list-item social__list-item_mr3">
                        <a class="social__list-item-link" href="#">
                            <img src="http://localhost/wordpress/arslilia/wp-content/themes/arslilia/images/ico-social-in.svg" alt="" srcset="">
                        </a>
                    </li>
                </ul>

            </div>


            <div class="footer__item">
                <?php

                $orderby = 'name';
                $order = 'asc';
                $hide_empty = false;

                $args = array(
                    'orderby' => $orderby,
                    'order' => $order,
                    'hide_empty' => $hide_empty,
                );

                $product_categories = get_terms( 'product_cat', $args );
                ?>
        
                <?php foreach($product_categories as $category) :?>
                    <a class="footer__link" href="<?php echo esc_attr( get_category_link( $category->term_id )); ?>"><?php echo $category->name; ?></a>
                <?php endforeach; ?>
            </div>


            <div class="footer__item">

                <address class="footer__address">г.Гродно, ул.Тимирязева 8 (ТЦ «Евроопт»)</address>
                <a class="footer__tel" href="tel:+375333992605">+375 (33) 399-26-05</a>
                <time class="footer__time">c 9:00 до 21:00</time>
                                                        
                <address class="footer__address">г.Гродно, ул. Я.Купалы 87 (ТРК «TRINITI»)</address>
                <a class="footer__tel" href="tel:+375333799579">+375 (33) 379-95-79</a>
                <time class="footer__time">c 9:00 до 21:00</time>
                                    
                <address class="footer__address">г.Гродно, ул.Дубко 17 (ТЦ «Old City»)</address>
                <a class="footer__tel" href="tel:+375333992607">+375 (33) 399-26-07</a>
                <time class="footer__time">c 9:00 до 22:00</time>

                <address class="footer__address">г. Гродно, ул. Огинского (ТЦ «Парус»)</address>
                <a class="footer__tel" href="tel:+375333992606">+375 (33) 399-26-06</a>
                <time class="footer__time">c 9:00 до 21:00</time>

            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="footer__copy">
            <p class="footer__copy-content">
            Частное торговое унитарное предприятие «АрсЛилия»<br>
            Юридический адрес: 230020, г. Гродно, ул. Кабяка, д.8б<br>
            Почтовый адрес: 230020, г. Гродно, ул. Кабяка, д.8б<br><br>
            
            УНП 591008172<br>
            ОКПО 301754704000<br>
            Банковские реквизиты:<br>
            BY95 AKBB 3012 0000 32933 3400 0000, филиал № 400 ГОУ ОАО «АСБ«Беларусбанк»,<br>
            код 152101752, г. Гродно, ул. Новооктябрьская,5
            </p>
    
            <img class="footer__logo-pay" src="<?php echo get_template_directory_uri(); ?>/images/copy.png" alt="" srcset="">
        </div>

        <div class="footer__flowers">
            <img class="footer___flowers-pic" src="<?php echo get_template_directory_uri(); ?>/images/footer-01.png" alt="" srcset="">
            <a class="footer___flowers-link" href="https://nastarte.by/">Дизайн и разработка — NaStarte</a>
            <img class="footer___flowers-pic" src="<?php echo get_template_directory_uri(); ?>/images/footer-02.png" alt="" srcset="">
        </div>
    </div>
</footer>

<?php wp_footer(); ?>

</body>
</html>

<script>
  AOS.init();
</script>