<?php
/*
Template Name: Contacts
*/

get_header();
?>

<main>
    <div class="wrapper">
    
    <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

    <h3 class="section__title">Контакты</h3>

        <section class="map">
            <div class="map__box">
                <div class="map__item">
                    <strong class="map__title">ТИМИРЯЗЕВА 8 (ТЦ «ЕВРООПТ»)</strong>
                    <a class="map__phone"href="tel:+375333992607">+375 (33) 399-26-07</a>
                    <time class="map__time">9:00 — 21:00</time>

                    <strong class="map__title">Я.КУПАЛЫ 87 (ТРК «TRINITI»)</strong>
                    <a class="map__phone"href="tel:+375333992607">+375 (33) 399-26-07</a>
                    <time class="map__time">9:00 — 21:00</time>

                    <strong class="map__title">ДУБКО 17 (ТЦ «OLD CITY»)</strong>
                    <a class="map__phone"href="tel:+375333992607">+375 (33) 399-26-07</a>
                    <time class="map__time">9:00 — 21:00</time>

                    <strong class="map__title">ОГИНСКОГО (ТЦ «ПАРУC»)</strong>
                    <a class="map__phone"href="tel:+375333992607">+375 (33) 399-26-07</a>
                    <time class="map__time">9:00 — 21:00</time>
                </div>
                <div class="map__item">
                    <div class="cart"></div>
                </div>
            </div>
        </section>
    </div>
</main>

<?php get_footer(); ?>