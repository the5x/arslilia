<?php
/*
Template Name: Post Type Artciles
Template Post Type: articles
*/

get_header();
?>

<main>        
    <section class="promo">
        <div class="wrapper">
            <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>
            
            <h3 class="section__title">Новости и события</h3>

            <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $args = array(
                    'post_type' => 'blog',
                    'posts_per_page' => 12,
                    'paged' => $paged,
                );
                
                $query = new WP_Query($args);
    
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
            ?>

            <div data-aos="fade-up" data-aos-duration="3000" class="promo__box">
                <div class="promo__item">
                    <?php
                        if ( has_post_thumbnail( $post->ID )) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                        } else {
                            $image = get_option( 'my_default_pic' );
                        }
                    ?>

                    <img class="promo__cover" src="<?php echo $image; ?>" alt="<?php the_title(); ?>" srcset="">
                </div>
                <div class="promo__item">                    
                    <a class="promo__title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>                    
                    <div class="promo__text"><?php the_excerpt(); ?></div>
                </div>
            </div>

            <?php endwhile; endif; wp_reset_postdata(); ?>

            </div>

            <div class="pagination">
                <?php the_posts_pagination(); ?>
            </div>

        </div>
    </section>
</main>

<?php get_footer(); ?>