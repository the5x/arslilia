<?php
/*
Template Name: About
*/

get_header();
?>

<main>        
    <section class="about">
        <div class="wrapper">
        
            <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

            <div class="about__box">
                <div class="about__item">
                    <h1 class="about__title">Меня зовут Татьяна</h1>
                    <p class="about__text">
                        Я основатель компании АрсЛилия. Первый свой магазин мы открыли в 2012 году. На протяжении девяти лет из маленького магазина мы выросли в сеть цветочных магазинов. Порой мне кажется, что цветы нас сами выбрали, а не мы их. Добро пожаловать на наш сайт.
                    </p>
                    <img class="about__flowers" src="<?php echo get_template_directory_uri(); ?>/images/owner-flowes.png" alt="" srcset="">
                </div>
                <div class="about__item">
                    <img class="about__owner-pic" src="<?php echo get_template_directory_uri(); ?>/images/owner-flower.png" alt="">
                    <div style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/owner.png');" class="about__pic"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="advantage advantage_ptpb10">

        <div class="wrapper">
            <ul style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/border-flower.png);" class="advantage__list">
                <li class="advantage__list-item">
                    <strong class="advantage__number">01</strong>
                    <strong class="advantage__title">10 лет на рынке</strong>
                </li>
                <li class="advantage__list-item advantage__list-item_empty"></li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">02</strong>
                    <strong class="advantage__title">Ежедневное обновление цветов</strong>
                </li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">03</strong>
                    <strong class="advantage__title">Более 50 сортов роз</strong>
                </li>
                <li class="advantage__list-item advantage__list-item_empty"></li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">04</strong>
                    <strong class="advantage__title">Букеты из экзотических цветов</strong>
                </li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">05</strong>
                    <strong class="advantage__title">Бесплатная доставка от 50 byn</strong>
                </li>
                <li class="advantage__list-item advantage__list-item_empty"></li>
                <li class="advantage__list-item">
                    <strong class="advantage__number">06</strong>
                    <strong class="advantage__title">Удобное расположение магазинов</strong>
                </li>
            </ul>
        </div>
    </section>

    <section class="history">

        <img class="advantage__ico-flower" src="<?php echo get_template_directory_uri(); ?>/images/ico-advantage-flower.png" alt="" srcset="">

        <div class="wrapper">
            
            <div class="history__box">
                <div class="history__item">
                    <strong class="history__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean</strong>
                    <p class="history__text">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis                        
                    </p>    
                </div>
                <div class="history__item">
                    <div style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/history-pic-01.png');" class="history__pic"></div>
                </div>
            </div>

            <div class="history__box">
                <div class="history__item">
                    <div style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/history-pic-02.png');" class="history__pic"></div>
                </div>
                <div class="history__item">
                    <img class="history__flower" src="<?php echo get_template_directory_uri(); ?>/images/history-background-flower.png" alt="" srcset="">
                    <strong class="history__title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean</strong>
                    <p class="history__text">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis                        
                    </p>    
                </div>
            </div>

        </div>
    </section>

    <section class="instagram">
        <div class="wrapper">
            <div class="instagram__box">
                <div class="instagram__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-01.png');">
                    <strong class="instagram__subtitle">Наш инстаграм</strong>
                </div>
                <div class="instagram__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-02.png');"></div>
                <div class="instagram__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-03.png');"></div>
                <div class="instagram__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-04.png');"></div>
                <div class="instagram__item instagram__item_fl2" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-05.png');"></div>
                <div class="instagram__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-06.png');"></div>
                <div class="instagram__item" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/instagram-08.png');">
                    <a class="instagram__subtitle" href="#">Наш инстаграм</a>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>