<?php get_header(); ?>

<main>

<section class="article">
    <div class="wrapper">

        <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>

        <article class="article">
            <?php
                if ( has_post_thumbnail( $post->ID )) {
                    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                } else {
                    $image = get_option( 'my_default_pic' );
                }
            ?>

            <div class="article__cover" style="background-image: url('<?php echo $image; ?>');"></div>
            
            <div class="article__box">
                <div class="article__item article__item_main">
                    <?php the_content(); ?>
                </div>

                <div class="article__item">

                <?php

                    $args = array(
                        'post_type' => 'blog',
                        'post_status' => 'publish',
                        'posts_per_page' => 3,
                        'orderby' => 'rand',
                    );
                    
                    $query = new WP_Query($args);
        
                    if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
                ?>

                    <div data-aos="fade-up" data-aos-duration="3000" class="blog__item blog__item_w100">
                        <?php
                            if ( has_post_thumbnail( $post->ID )) {
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                            } else {
                                $image = get_option( 'my_default_pic' );
                            }
                        ?>

                        <div class="blog__cover" style="background-image: url('<?php echo $image ?>');">
                            <time class="blog__time"><?php echo get_the_date( 'd.m.Y' ); ?></time>
                        </div>
                        <a class="blog__link" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                    </div>

                <?php endwhile; endif; wp_reset_postdata(); ?>
                </div>
            </div>
        </article>
    </div>
</section>

</main>

</main>

<?php get_footer(); ?>