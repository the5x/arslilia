<?php
/*
Template Name: Post Type Stock
Template Post Type: blog
*/

get_header();
?>


<main>        
    <section class="promo">
        <div class="wrapper">
            <?php if( function_exists('kama_breadcrumbs') ) kama_breadcrumbs(' » '); ?>
            
            <h3 class="section__title">Акции и скидки</h3>

            <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                $args = array(
                    'post_type' => 'stocks',
                    'posts_per_page' => 12,
                    'paged' => $paged,
                );
                
                $query = new WP_Query($args);
    
                if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
            ?>

            <div data-aos="fade-up" data-aos-duration="3000" class="promo__box">
                <div class="promo__item">
                    <?php
                        if ( has_post_thumbnail( $post->ID )) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full', false )[0];
                        } else {
                            $image = get_option( 'my_default_pic' );
                        }
                    ?>

                    <img class="promo__cover" src="<?php echo $image; ?>" alt="<?php the_title(); ?>" srcset="">
                </div>
                <div class="promo__item">

                    <?php
                        $terms = get_the_terms( $post->ID, 'product_cat' );
                        $term = array_pop($terms);

                        if ( $term->slug ):
                    ?>
                        <a class="promo__title" href="<?php echo site_url(); ?>/categories/<?php echo $term->slug; ?>"><?php the_title(); ?></a>
                    <?php else: ?>
                        <h3 class="promo__title"><?php the_title(); ?></h3>
                    <?php endif; ?>
                    
                    <div class="promo__text"><?php the_excerpt(); ?></div>
                </div>
            </div>

            <?php endwhile; endif; wp_reset_postdata(); ?>

            </div>

            <div class="pagination">
                <?php the_posts_pagination(); ?>
            </div>

        </div>
    </section>
</main>

<?php get_footer(); ?>